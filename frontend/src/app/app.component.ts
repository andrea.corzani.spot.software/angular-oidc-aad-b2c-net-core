import { OAuthErrorEvent, OAuthService } from 'angular-oauth2-oidc';

import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';

export interface OAuthErrorEventParams {
  error: string;
  error_description: string;
  state: string;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  apiResult;
  constructor(readonly oauthService: OAuthService, private httpClient: HttpClient) {


    this.oauthService.configure({
      issuer: 'https://myejob.b2clogin.com/myejob.onmicrosoft.com/v2.0/',
      loginUrl: 'https://myejob.b2clogin.com/myejob.onmicrosoft.com/B2C_1A_force_reset_signin/v2.0',
      // logoutUrl: 'https://myejob.b2clogin.com/myejob.onmicrosoft.com/oauth2/v2.0/logout?p=B2C_1A_force_reset_signin',
      redirectUri: window.location.origin,
      clientId: 'dad81d86-02e4-4d5d-8d4b-17cf9a2c3e5b',
      scope: 'openid offline_access https://myejob.onmicrosoft.com/api/access_as_user',
      responseType: 'code',
      showDebugInformation: true,
      strictDiscoveryDocumentValidation: false,
      skipIssuerCheck: true,
      oidc: true,
      useSilentRefresh: true,
      silentRefreshTimeout: 5000, // For faster testing
      timeoutFactor: 0.5, // For faster testing
      // useIdTokenHintForSilentRefresh: true,
      silentRefreshRedirectUri: window.location.origin + '/silent-renew.html',
    });

    const url = 'https://myejob.b2clogin.com/myejob.onmicrosoft.com/v2.0/.well-known/openid-configuration?p=B2C_1A_force_reset_signin';

    this.oauthService
      .loadDiscoveryDocument(url)
      .then(resp => this.oauthService.tryLoginCodeFlow())
      .then(_ => {
        if (!this.oauthService.hasValidAccessToken()) {
          this.oauthService.initLoginFlow();
        }
      })
      .then(r => this.oauthService.setupAutomaticSilentRefresh())
      .catch(err => {
        console.log('error: ', err);
      });

    this.oauthService.events.subscribe(e => {
      if (e instanceof OAuthErrorEvent) {
        const parm = e.params as OAuthErrorEventParams;
        if (parm.error === 'access_denied' && parm.error_description.includes('AADB2C90118')) {
          // redirect to forgot password flow
          const resetUrl = 'https://myejob.b2clogin.com/myejob.onmicrosoft.com/oauth2/v2.0/authorize?' +
            'p=B2C_1_ChangePassword' +
            '&client_id=dad81d86-02e4-4d5d-8d4b-17cf9a2c3e5b' +
            '&nonce=defaultNonce' +
            '&redirect_uri=' + window.location.origin +
            '&scope=openid' +
            '&response_type=code' +
            '&prompt=login' +
            '&code_challenge=##################&code_challenge_method=S256';


          const redir = resetUrl;
          window.location.href = redir;
        } else if (parm.error === 'access_denied' && parm.error_description.includes('AADB2C90091')) {
          // user has cancelled out of password reset
          this.oauthService.initLoginFlow();
        }
      }
    });
  }

  forceRefresh() {
    this.oauthService.refreshToken();
  }

  async callAdminApi() {
    try {
      this.apiResult = await this.httpClient.get('/api/users/admin').toPromise();
    } catch (err) {
      let statusCode = JSON.parse(JSON.stringify(err)).status;
      if(statusCode == 403) {
        this.apiResult = "Unauthorized user! Only admins can call this API"
      } else {
        this.apiResult = err;
      }
    }
  }

  async callMemberApi() {
    let claims = JSON.parse(JSON.stringify(this.oauthService.getIdentityClaims()));
    try {
      this.apiResult = await this.httpClient.get('/api/users/member').toPromise();
    } catch (err) {
      let statusCode = JSON.parse(JSON.stringify(err)).status;
      if(statusCode == 403) {
        this.apiResult = "Unauthorized user! You have to be logged in"
      } else {
        this.apiResult = err;
      }
    }
  }

  logout() {
    this.oauthService.logOut();
  }
}
