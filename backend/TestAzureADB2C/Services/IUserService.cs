﻿using Microsoft.Graph;
using System.Collections.Generic;

namespace TestAzureADB2C.Services {
    public interface IUserService {

        GraphServiceClient GetGraphServiceClient();
        IDictionary<string, object> ModifyCustomClaims(int companyId, string employmentDate, string fiscalCode, string phone, string role, bool mustResetPassword);
        string getSelectString();
    }
}
