﻿using Microsoft.Graph;
using Microsoft.Graph.Auth;
using Microsoft.Identity.Client;
using System.Collections.Generic;
using TestAzureADB2C.Models;

namespace TestAzureADB2C.Services {
    public class UserService : IUserService {
        static readonly AppSettings config = AppSettingsFile.ReadFromJsonFile();

        private const string customAttributeCompanyId = "CompanyId";
        private const string customAttributeEmploymentDate = "EmploymentDate";
        private const string customAttributeFiscalCode = "FiscalCode";
        private const string customAttributePhone = "Phone";
        private const string customAttributeRole = "Role";
        private const string customAttributeMustResetPsw = "mustResetPassword";

        private static readonly Helpers.B2CCustomAttributeHelper helper = new Helpers.B2CCustomAttributeHelper(config.B2CExtensionClientId);
        private readonly string companyIdAttributeName = helper.GetCompleteAttributeName(customAttributeCompanyId);
        private readonly string employmentDateAttributeName = helper.GetCompleteAttributeName(customAttributeEmploymentDate);
        private readonly string fiscalCodeAttributeName = helper.GetCompleteAttributeName(customAttributeFiscalCode);
        private readonly string phoneAttributeName = helper.GetCompleteAttributeName(customAttributePhone);
        private readonly string roleAttributeName = helper.GetCompleteAttributeName(customAttributeRole);
        private readonly string resetPswAttributeName = helper.GetCompleteAttributeName(customAttributeMustResetPsw);

        public IDictionary<string, object> ModifyCustomClaims(int companyId, string employmentDate, string fiscalCode, 
                                                              string phone, string role, bool mustResetPassword) {

            return new Dictionary<string, object> {
                {companyIdAttributeName, companyId},
                {employmentDateAttributeName, employmentDate},
                {fiscalCodeAttributeName, fiscalCode},
                {phoneAttributeName, phone},
                {roleAttributeName, role},
                {resetPswAttributeName, mustResetPassword}
            };
        }

        public GraphServiceClient GetGraphServiceClient() {
            IConfidentialClientApplication confidentialClientApplication = ConfidentialClientApplicationBuilder
                .Create(config.ClientId)
                .WithTenantId(config.TenantId)
                .WithClientSecret(config.ClientSecret)
                .Build();

            ClientCredentialProvider authProvider = new ClientCredentialProvider(confidentialClientApplication);

            return new GraphServiceClient(authProvider);
        }

        public string getSelectString() {
            return $"id," +
                   $"givenName," +
                   $"surName," +
                   $"displayName, " +
                   $"city, " +
                   $"identities," +
                   $"{companyIdAttributeName}," +
                   $"{employmentDateAttributeName}," +
                   $"{fiscalCodeAttributeName}," +
                   $"{phoneAttributeName}," +
                   $"{roleAttributeName}," +
                   $"{resetPswAttributeName}";
        }
    }
}
