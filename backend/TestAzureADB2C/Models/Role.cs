﻿namespace TestAzureADB2C.Models {
    enum Role {
        Employee = 0,
        Freelance = 1,
        Company = 2,
        MedicalConsultant = 3,
        SuperAdmin = 4
    }
}
