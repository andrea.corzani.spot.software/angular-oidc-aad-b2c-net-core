using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.AzureADB2C.UI;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Logging;
using System.IdentityModel.Tokens.Jwt;
using TestAzureADB2C.Models;
using TestAzureADB2C.Services;

namespace TestAzureADB2C {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            IdentityModelEventSource.ShowPII = true;
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            // IdentityModelEventSource.ShowPII = true;

            services.AddScoped<IUserService, UserService>();

            services.AddCors(options => {
                options.AddPolicy("AllowAllOrigins",
                    builder => {
                        builder
                            .AllowCredentials()
                            .WithOrigins(
                                "http://localhost:4200")
                            .SetIsOriginAllowedToAllowWildcardSubdomains()
                            .AllowAnyHeader()
                            .AllowAnyMethod();
                    });
            });
            services.AddAuthentication(AzureADB2CDefaults.BearerAuthenticationScheme)
                .AddAzureADB2CBearer(options => Configuration.Bind("AzureAdB2C", options));
            services.AddAuthorization(options => {
                options.AddPolicy("AdminRole", policy =>
                                  policy.RequireClaim("extension_Role", ((int)Role.SuperAdmin).ToString()));
                options.AddPolicy("MemberRole", policy =>
                                  policy.RequireClaim("extension_Role", ((int)Role.Employee).ToString(),
                                                                        ((int)Role.Freelance).ToString(),
                                                                        ((int)Role.Company).ToString(),
                                                                        ((int)Role.MedicalConsultant).ToString(),
                                                                        ((int)Role.SuperAdmin).ToString()));
            });
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            if(env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("AllowAllOrigins");
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });
        }
    }
}
