﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Graph;
using Newtonsoft.Json;
using TestAzureADB2C.Services;

namespace TestAzureADB2C.Controllers {
    [Authorize(Policy = "MemberRole")]
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase {

        private readonly ILogger<UsersController> _logger;
        private readonly IUserService _userService;

        public UsersController(ILogger<UsersController> logger, IUserService userService) {
            _logger = logger;
            _userService = userService;
        }
        [Authorize(Policy = "AdminRole")]
        [HttpGet("admin")]
        public async Task<string> GetAllAsync() {

            //if(HttpContext.User.Identity is ClaimsIdentity identity) {
            //    await ModifyUser(identity.FindFirst("sub").Value, _userService.GetGraphServiceClient());
            //}
            _logger.LogInformation("GetAllUsers call");
            return await GetAllUsersAsync(_userService.GetGraphServiceClient());
        }

        [HttpGet("member")]
        public async Task<string> GetUserMemberAsync() {

            if(HttpContext.User.Identity is ClaimsIdentity identity) {
                return await GetUserBySignInName(identity.FindFirst("sub").Value, _userService.GetGraphServiceClient());
            } else {
                return "ERROR";
            }
        }

        private async Task<User> ModifyUser(string userId, GraphServiceClient graphClient) {

            var user = new User {
                City = "Cesena",
                AdditionalData = _userService.ModifyCustomClaims(1, "2020-10-27", "1234567891234567", "2222222222", "4", true)
            };
            return await graphClient.Users[userId].Request().UpdateAsync(user);
        }

        private async Task<string> GetAllUsersAsync(GraphServiceClient graphClient) {

            return JsonConvert.SerializeObject(await graphClient.Users
                                                                .Request()
                                                                .Select(_userService.getSelectString())
                                                                .GetAsync());
        }

        private async Task<string> GetUserBySignInName(string userId, GraphServiceClient graphClient) {

            return JsonConvert.SerializeObject(await graphClient.Users[userId]
                                                                .Request()
                                                                .Select(_userService.getSelectString())
                                                                .GetAsync());
        }
    }
}
